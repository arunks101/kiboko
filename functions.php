<?php
/**
 * Proper way to enqueue scripts and styles
 */

function mod_mce($initArray) {
	$initArray['verify_html'] = false;
	return $initArray;
}
add_filter('tiny_mce_before_init', 'mod_mce');
?>
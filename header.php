<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php elegant_description(); ?>
	<?php elegant_keywords(); ?>
	<?php elegant_canonical(); ?>

	<?php do_action( 'et_head_meta' ); ?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php $template_directory_uri = get_template_directory_uri(); ?>
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
	<![endif]-->
<script src="<?php echo esc_url( $template_directory_uri . '-child/js/home-en.js"' ); ?>" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script type="text/javascript">
		document.documentElement.className = 'js';
		$(document).ready(function(){
			
		$("#nav_header").click(function(){
			if($( "#top-menu-nav" ).hasClass( "open" )){
				$("#top-menu-nav").removeClass("open");
			}else{
				$("#top-menu-nav").addClass("open");
			}
    		
		}); 
		}); 
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="page-container">
<?php
	if ( is_page_template( 'page-template-blank.php' ) ) {
		return;
	}

	$et_secondary_nav_items = et_divi_get_top_nav_items();

	$et_phone_number = $et_secondary_nav_items->phone_number;

	$et_email = $et_secondary_nav_items->email;

	$et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;

	$show_header_social_icons = $et_secondary_nav_items->show_header_social_icons;

	$et_secondary_nav = $et_secondary_nav_items->secondary_nav;

	$et_top_info_defined = $et_secondary_nav_items->top_info_defined;

	$et_slide_header = 'slide' === et_get_option( 'header_style', 'left' ) || 'fullscreen' === et_get_option( 'header_style', 'left' ) ? true : false;
?>

	<?php if ( $et_top_info_defined && ! $et_slide_header || is_customize_preview() ) : ?>
		<div id="top-header"<?php echo $et_top_info_defined ? '' : 'style="display: none;"'; ?>>
			<div class="container clearfix">

			<?php if ( $et_contact_info_defined ) : ?>

				<div id="et-info">
				<?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
					<span id="et-info-phone"><?php echo et_sanitize_html_input_text( $et_phone_number ); ?></span>
				<?php endif; ?>

				<?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
					<a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
				<?php endif; ?>

				<?php
				if ( true === $show_header_social_icons ) {
					get_template_part( 'includes/social_icons', 'header' );
				} ?>
				</div> <!-- #et-info -->

			<?php endif; // true === $et_contact_info_defined ?>

				<div id="et-secondary-menu">
				<?php
					if ( ! $et_contact_info_defined && true === $show_header_social_icons ) {
						get_template_part( 'includes/social_icons', 'header' );
					} else if ( $et_contact_info_defined && true === $show_header_social_icons ) {
						ob_start();

						get_template_part( 'includes/social_icons', 'header' );

						$duplicate_social_icons = ob_get_contents();

						ob_end_clean();

						printf(
							'<div class="et_duplicate_social_icons">
								%1$s
							</div>',
							$duplicate_social_icons
						);
					}

					if ( '' !== $et_secondary_nav ) {
						echo $et_secondary_nav;
					}

					et_show_cart_total();
				?>
				</div> <!-- #et-secondary-menu -->

			</div> <!-- .container -->
		</div> <!-- #top-header -->
	<?php endif; // true ==== $et_top_info_defined ?>

	<?php if ( $et_slide_header || is_customize_preview() ) : ?>
		<div class="et_slide_in_menu_container">
			<?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) || is_customize_preview() ) { ?>
				<span class="mobile_menu_bar et_toggle_fullscreen_menu"></span>
			<?php } ?>

			<?php
				if ( $et_contact_info_defined || true === $show_header_social_icons || false !== et_get_option( 'show_search_icon', true ) || class_exists( 'woocommerce' ) || is_customize_preview() ) { ?>
					<div class="et_slide_menu_top">

					<?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) ) { ?>
						<div class="et_pb_top_menu_inner">
					<?php } ?>
			<?php }

				if ( true === $show_header_social_icons ) {
					get_template_part( 'includes/social_icons', 'header' );
				}

				et_show_cart_total();
			?>
			<?php if ( false !== et_get_option( 'show_search_icon', true ) || is_customize_preview() ) : ?>
				<?php if ( 'fullscreen' !== et_get_option( 'header_style', 'left' ) ) { ?>
					<div class="clear"></div>
				<?php } ?>
				<form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
						printf( '<input type="search" class="et-search-field" placeholder="%1$s" placeholder="%2$s" name="s" title="%3$s" />',
							esc_attr__( 'Search &hellip;', 'Divi' ),
							get_search_query(),
							esc_attr__( 'Search for:', 'Divi' )
						);
					?>
					<button type="submit" id="searchsubmit_header"></button>
				</form>
			<?php endif; // true === et_get_option( 'show_search_icon', false ) ?>

			<?php if ( $et_contact_info_defined ) : ?>

				<div id="et-info">
				<?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
					<span id="et-info-phone"><?php echo et_sanitize_html_input_text( $et_phone_number ); ?></span>
				<?php endif; ?>

				<?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
					<a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
				<?php endif; ?>
				</div> <!-- #et-info -->

			<?php endif; // true === $et_contact_info_defined ?>
			<?php if ( $et_contact_info_defined || true === $show_header_social_icons || false !== et_get_option( 'show_search_icon', true ) || class_exists( 'woocommerce' ) || is_customize_preview() ) { ?>
				<?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) ) { ?>
					</div> <!-- .et_pb_top_menu_inner -->
				<?php } ?>

				</div> <!-- .et_slide_menu_top -->
			<?php } ?>

			<div class="et_pb_fullscreen_nav_container">
				<?php
					$slide_nav = '';
					$slide_menu_class = 'et_mobile_menu';

					$slide_nav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'echo' => false, 'items_wrap' => '%3$s' ) );
					$slide_nav .= wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'container' => '', 'fallback_cb' => '', 'echo' => false, 'items_wrap' => '%3$s' ) );
				?>

				<ul id="mobile_menu_slide" class="<?php echo esc_attr( $slide_menu_class ); ?>">

				<?php
					if ( '' == $slide_nav ) :
				?>
						<?php if ( 'on' == et_get_option( 'divi_home_link' ) ) { ?>
							<li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>
						<?php }; ?>

						<?php show_page_menu( $slide_menu_class, false, false ); ?>
						<?php show_categories_menu( $slide_menu_class, false ); ?>
				<?php
					else :
						echo( $slide_nav );
					endif;
				?>

				</ul>
			</div>
		</div>
	<?php endif; // true ==== $et_slide_header ?>

		<header id="main-header" data-height-onload="<?php echo esc_attr( et_get_option( 'menu_height', '66' ) ); ?>">
			<div class="container-fluid clearfix et_menu_container">
			<?php
				$logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && '' != $user_logo
					? $user_logo
					: $template_directory_uri . '/images/logo.png';
			?>
				<div class="logo_container">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<!-- <img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="logo" data-height-percentage="<?php echo esc_attr( et_get_option( 'logo_height', '54' ) ); ?>" /> -->
						<svg xml:space="preserve" enable-background="new 0 0 39.1 39.1" viewBox="0 0 39.1 39.1" y="0px" x="0px" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" id="logotopMobile" version="1.1" style="opacity: 1;">
                        <g>
                            <path d="M8.1,6.4v13.1" stroke-miterlimit="10" stroke="#FFFFFF" fill="none" class="logotop-path" style="stroke-dasharray: 13.1, 13.1; stroke-dashoffset: 0;"/>
                            <path d="M24.5,19.5H8.1V6.4h16.3c3.6,0,6.5,2.9,6.5,6.5V13
                                C31,16.6,28.1,19.5,24.5,19.5z" stroke-miterlimit="10" stroke="#FFFFFF" fill="none" class="logotop-path" style="stroke-dasharray: 66.2962, 66.2962; stroke-dashoffset: 0;"/>
                            <path d="M8.1,32.7V19.5h2.3l9.1-13.1" stroke-miterlimit="10" stroke="#FFFFFF" fill="none" class="logotop-path" style="stroke-dasharray: 31.4506, 31.4506; stroke-dashoffset: 0;"/>
                            <path d="M13.9,19.5" stroke-miterlimit="10" stroke="#FFFFFF" fill="none" class="logotop-path" style="stroke-dasharray: 0, 0; stroke-dashoffset: 0;"/>
                            <path d="M13.9,32.7V19.5h2.3l9.1-13.1" stroke-miterlimit="10" stroke="#FFFFFF" fill="none" class="logotop-path" style="stroke-dasharray: 31.4506, 31.4506; stroke-dashoffset: 0;"/>
                            <path d="M16.2,19.5l9.1,13.1" stroke-miterlimit="10" stroke="#FFFFFF" fill="none" class="logotop-path" style="stroke-dasharray: 15.9505, 15.9505; stroke-dashoffset: 0;"/>
                            <path d="M24.5,32.7H8.1V19.5h16.3c3.6,0,6.5,2.9,6.5,6.5v0.1
                                C31,29.8,28.1,32.7,24.5,32.7z" stroke-miterlimit="10" stroke="#FFFFFF" fill="none" class="logotop-path" style="stroke-dasharray: 66.485, 66.485; stroke-dashoffset: 0;"/>
                            <path d="M38.6,38.6H0.5V0.5h38.1V38.6z" stroke-miterlimit="10" stroke="#FFFFFF" fill="none" class="logotop-path" style="stroke-dasharray: 152.4, 152.4; stroke-dashoffset: 0;"/>
                        </g>
                    <desc>Created with Snap</desc><defs/></svg>

                    <svg xml:space="preserve" enable-background="new 49.4 65.3 154.9 26.1" viewBox="49.4 65.3 154.9 26.1" y="0px" x="0px" class="hidden-xs" id="logotopDesktop" version="1.1" style="opacity: 1;">
                        <path d="M112.4,72.9v-0.1c0-4.1-3.4-7.4-7.4-7.4H92.1v10.9v4.1v10.9H105c4.1,0,7.4-3.4,7.4-7.4v-0.1
                            c0-2.1-0.9-4-2.3-5.4C111.6,76.9,112.4,75,112.4,72.9z M108.5,83.7v0.1c0,1.9-1.5,3.4-3.4,3.4h-9v-7h9
                            C106.9,80.2,108.5,81.8,108.5,83.7z M108.5,72.8v0.1c0,1.9-1.5,3.4-3.4,3.4h-9v-7h9C106.9,69.4,108.5,70.9,108.5,72.8z M77.7,65.3h4
                            v25.9h-4V65.3z M131.7,65.3c-7.1,0-12.9,5.8-12.9,13s5.8,13,12.9,13c7.1,0,13-5.8,13-13S138.8,65.3,131.7,65.3z M131.7,87.3
                            c-5,0-9-3.9-9-9c0-5,3.9-9,9-9c4.9,0,9,4,9,9S136.6,87.3,131.7,87.3z M191.2,65.3c-7.1,0-12.9,5.8-12.9,13s5.8,13,12.9,13
                            c7.1,0,13-5.8,13-13C204.2,71.1,198.4,65.3,191.2,65.3z M191.2,87.3c-5,0-9-3.9-9-9c0-5,3.9-9,9-9c4.9,0,9,4,9,9
                            S196.2,87.3,191.2,87.3z M59.4,79.3l12,12h-5.7l-11.1-11h-1.3v11h-4v-11l0,0v-4l0,0v-11h4v11h1.3l11.1-11h5.7l-12,12l-1,0.9
                            L59.4,79.3z M162.8,79.3l12,12h-5.7l-11.1-11h-1.3v11h-4v-11l0,0v-4l0,0v-11h4v11h1.3l11.1-11h5.7l-12,12l-0.9,0.9L162.8,79.3z" fill-opacity="0" class="logoDesktopPath" fill="#FFFFFF" style="stroke-dasharray: 770.925, 770.925; stroke-dashoffset: 770.925; fill-opacity: 1;"/>
                    <desc>Created with Snap</desc><defs/></svg>
					</a>
				</div>
				<div id="et-top-navigation" data-height="<?php echo esc_attr( et_get_option( 'menu_height', '66' ) ); ?>" data-fixed-height="<?php echo esc_attr( et_get_option( 'minimized_menu_height', '40' ) ); ?>">
				<div id="nav_header">Menu</div>
					<?php if ( ! $et_slide_header || is_customize_preview() ) : ?>
						<nav id="top-menu-nav">
						<?php
							$menuClass = 'nav';
							if ( 'on' == et_get_option( 'divi_disable_toptier' ) ) $menuClass .= ' et_disable_top_tier';
							$primaryNav = '';

							$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'top-menu', 'echo' => false ) );

							if ( '' == $primaryNav ) :
						?>
							<ul id="top-menu" class="<?php echo esc_attr( $menuClass ); ?>">
								<?php if ( 'on' == et_get_option( 'divi_home_link' ) ) { ?>
									<li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>
								<?php }; ?>

								<?php show_page_menu( $menuClass, false, false ); ?>
								<?php show_categories_menu( $menuClass, false ); ?>
							</ul>
						<?php
							else :
								echo( $primaryNav );
							endif;
						?>
						</nav>
					<?php endif; ?>

					<?php
					if ( ! $et_top_info_defined && ( ! $et_slide_header || is_customize_preview() ) ) {
						et_show_cart_total( array(
							'no_text' => true,
						) );
					}
					?>

					<?php if ( $et_slide_header || is_customize_preview() ) : ?>
						<span class="mobile_menu_bar et_pb_header_toggle et_toggle_<?php echo esc_attr( et_get_option( 'header_style', 'left' ) ); ?>_menu"></span>
					<?php endif; ?>

					<?php if ( ( false !== et_get_option( 'show_search_icon', true ) && ! $et_slide_header ) || is_customize_preview() ) : ?>
					<div id="et_top_search">
						<span id="et_search_icon"></span>
					</div>
					<?php endif; // true === et_get_option( 'show_search_icon', false ) ?>

					<?php do_action( 'et_header_top' ); ?>
				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
			<div class="et_search_outer">
				<div class="container et_search_form_container">
					<form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
						printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
							esc_attr__( 'Search &hellip;', 'Divi' ),
							get_search_query(),
							esc_attr__( 'Search for:', 'Divi' )
						);
					?>
					</form>
					<span class="et_close_search_field"></span>
				</div>
			</div>
		</header> <!-- #main-header -->

		<div id="et-main-area">